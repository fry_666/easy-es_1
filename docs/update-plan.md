## 版本更新计划
> 版本规划中需求为

- ~~0.9.4 (2022年1月上旬前,已发布)~~
   1. ~~新增es配置项,可以灵活配置请求超时时间,最大连接数等一系列深度配置~~
   1. ~~修复查询列表时,id为同一个的缺陷~~
   1. ~~修复查询条件为in/notIn查询条件未生效的缺陷~~
   1. ~~设置默认查询条目数为1w条~~
   1. ~~文档更新,引入查询条目数说明及如何设置~~
- ~~0.9.5 (2022年4月前,大部分核心功能已提前发布)~~
   1. ~~优化代码中一切可以优化的地方~~
   1. ~~引入Lambda风格地理位置查询api~~
   1. 引入自动创建索引模块,索引可配置策略是否自动创建,用户可灵活选择自动托管还是手动创建. 

@2022-02-16 此需求因es索引更新会导致重建问题(核心原因),合理性待后续评估,暂时延后待排期

   4. 增强原@TableField字段注解,引入mapping类型的自动映射和手动指定,分词器指定等.

@2022-02-16 时间原因延期,此需求因es索引更新会导致重建问题(核心原因),合理性待后续评估,暂时延后待排期,已发布通过API方式指定和更新分词器功能作为平替版.<br />~~本版本已上线通过API方式指定和更新分词器,作为此功能的平替版先上线了~~

-  ~~0.9.6(2022年3月中旬前)~~
   1. ~~引入性能测试模块~~
   1. ~~接口功能测试覆盖率100% (已完成)~~
   1. ~~修缮所有被反馈或者存在缺陷或设计不合理的地方~~
   1. ~~文档更新,引入顾虑粉碎模块(已提前发布)~~
   1. ~~ID支持用户自定义传入,由GITEE网友@~~[~~青山~~](https://gitee.com/luoqy)~~在2月提出,这样可以在插入数据时,如果ID重复,则自动更新文档,ID不重复时才新增. (此需求合理性待大家评估.目前已提供了UUID和ES默认ID两种ID生成方式)~~
   1. ~~新增混合查询文档,帮助用户解决EE功能不支持问题~~
- 0.9.7(2022年6月前,有功能可能提前发布,有功能可能延后至下迭代,但大的时间线不会变)
   1. 聚合模块功能支持更多ES独有的聚合功能 答疑群内网友提出
   1. 聚合功能进一步强化,支持更多种类型的聚合
   1. 地理位置查询的排序功能进一步强化,支持更多场景
   1. 提供多数据源的支持(答疑群内用户"极光"提出)
   1. 提供对主键的多类型支持(Integer,Long等,不需要转为String,群内"大仙儿"提出)
   1. 对or(),or(Function<Param, Param> func)进一步优化,提升用户体验
   1. 引入插件Module,开发者可以定制自己所需插件,开发者自定义,目前该模块已有贡献者一个插件 @璐先生,代码在dev分支,喜欢尝鲜的用户可以下载尝试,该插件主要提供AOP前置拦截增强功能. 也欢迎更多开发者来贡献插件

##    需求池
> 需求池中需求待排期,看迭代规划,择期上线

   1. xpack本地秘钥型权限校验模块配置功能  由网友@周立波邮件本人提出此需求支持
   1. Percolate反向检索支持,由GItee用户Earl提出
   1. 通过自定义注解懒人一键托管索引功能,支持通过配置开启/关闭此功能

- 持续更新中...
